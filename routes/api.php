<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\{
    MainController,
    AuthController,
    CartController,
    CompanyController,
    CategoryController,
    ProductController,
};

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/main', [MainController::class, 'index']);
Route::get('/category/{category}', [MainController::class, 'category']);
Route::get('/product/{product}', [MainController::class, 'product']);
Route::post('/cart/store', [CartController::class, 'store']);


Route::group(['prefix' => 'auth'], function () {
    Route::post('/register',[AuthController::class, 'register']);
    Route::post('/login',[AuthController::class, 'login']);
    Route::post('/password_update',[AuthController::class, 'changePassword']);
});

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::apiResource('categories', CategoryController::class);
    Route::apiResource('companies', CompanyController::class);
    Route::apiResource('products', ProductController::class);
    Route::post('/logout', [AuthController::class, 'logout']);
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});