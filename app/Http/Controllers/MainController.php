<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\{
    Company,
    Category,
    Product,
};

class MainController extends Controller
{
    public function index(Request $request){
        $company = Company::with('categories')->find($request->company_id);
        return response($company, 200);
    }

    public function category(Category $category){
        $category->products;
        return response($category, 200);
    }

    public function product(Product $product){
        return response($product, 200);
    }

    
}
