<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Models\Product;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = auth()->user()->company->products;

        return response($products, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $request->validate([
            'title' => 'required|string',
            'price' => 'required|integer',
            'image' => 'required|string',
            'category_id' => 'required|integer',
            'is_pinned' => 'boolean'
        ]);
        $fields['category_id'] = auth()->user()->company_id;

        $product = Product::create($fields);

        $response = [
            'message' => 'Новый продукт был создан',
            'product' => $product
        ];

        return response($response, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {

        if(auth()->user()->company_id != $product->category->company_id) return response(['message'=>'Данный продукт не принадлежит вам']);

        return response($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $fields = $request->validate([
            'title' => 'string',
            'price' => 'integer',
            'image' => 'string',
            'is_pinned' => 'boolean',
            'category_id' => 'integer',
        ]);

        $product->update($fields);

        $response = [
            'message' => 'продукт был изменен',
            'product' => $product,
        ];

        return response($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {

        if(auth()->user()->company_id != $product->category->company_id) return response(['message'=>'Данный товар не принадлежит вам']);

        $product->delete();

        $response = ['message' => 'Товар был удален'];

        return response($response, 200);
    }
}
