<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Models\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::where('company_id', auth()->user()->company_id)->get();

        return response($categories, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $request->validate([
            'name' => 'required|string',
            'image' => 'nullable|string'
        ]);
        $fields['company_id'] = auth()->user()->company_id;

        $category = Category::create($fields);

        $response = [
            'message' => 'Новая категория была создана',
            'category' => $category
        ];

        return response($response, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  Category $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {

        if(auth()->user()->company_id != $category->company_id) return response(['message'=>'Данная категория не принадлежит вам']);

        return response($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $fields = $request->validate([
            'name' => 'string',
            'image' => 'nullable|string'
        ]);

        $category->update($fields);

        $response = [
            'message' => 'Категория была изменена',
            'category' => $category,
        ];

        return response($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Category $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {

        if(auth()->user()->company_id != $category->company_id) return response(['message'=>'Данная категория не принадлежит вам']);

        $category->delete();

        $response = ['message' => 'Категория была удалена'];

        return response($response, 200);
    }
}
