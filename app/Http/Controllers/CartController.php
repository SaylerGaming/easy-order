<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\{
    Cart,
    Order
};

class CartController extends Controller
{
    public function store(Request $request){
        $cart = Cart::create([
            'table_number' => $request->table_number,
            'status' => 'Заказ принят',
            'total_price' => $request->total_price,
            'company_id' => $request->company_id,
        ]);

        foreach($request->products as $product) {
            Order::create([
                'quantity' => $product['quantity'],
                'product_id' => $product['id'],
                'cart_id' => $cart->id,
            ]);
        }

        return response('Заказ был принят', 201);
    }
}
