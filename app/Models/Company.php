<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];

    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    public function products()
    {
        return $this->hasManyThrough(Product::class, Category::class);
    }

    public function users()
    {        
        return $this->hasMany(User::class);
    }

    public function carts()
    {        
        return $this->hasMany(Cart::class);
    }

    public function orders()
    {
        return $this->hasManyThrough(Order::class, Cart::class);
    }
}
