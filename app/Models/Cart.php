<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;

    protected $fillable = [
        'table_number',
        'status',
        'total_price',
        'company_id'
    ];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
