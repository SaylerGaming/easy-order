<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'title' => $this->faker->words($this->faker->numberBetween(2, 4), true),
            'price' => $this->faker->numberBetween(1000,3000),
            'image' => $this->faker->imageUrl(500, 500),
            'category_id' => $this->faker->numberBetween(1, 3),
            'is_pinned' => $this->faker->numberBetween(0,1),
        ];
    }
}
