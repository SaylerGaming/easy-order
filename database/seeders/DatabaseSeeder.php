<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\models\Company::factory(1)->create();
        \App\Models\User::factory(3)->create();
        \App\models\Category::factory(3)->create();
        \App\Models\Product::factory(21)->create();
    }
}
